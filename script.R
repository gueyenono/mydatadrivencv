datadrivencv::use_datadriven_cv(
  full_name = "Ghislain Nono Gueye, Ph.D. ",
  data_location = "data/",
  pdf_location = here::here(),
  html_location = here::here(),
  source_location = here::here()
)
